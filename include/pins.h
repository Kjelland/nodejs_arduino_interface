
#ifndef PINS_H_
#define PINS_H_
#include "Arduino.h"


const int pitchMotor1 = 3;
const int pitchMotor2 = 5;
const int pitchMotor3 = 6;

const int left_motor_A = 11;
const int left_motor_B = 10;
const int left_motor_C = 9;


#endif /* PINS_H_ */
