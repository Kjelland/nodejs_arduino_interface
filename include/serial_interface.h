//
// Created by magnus on 02.11.2021.
//

#ifndef SERIAL_INTERFACE__SERIAL_INTERFACE_H_
#define SERIAL_INTERFACE__SERIAL_INTERFACE_H_

#include "Arduino.h"
//include "iostream"
//#include "libserial/SerialStream.h"

//using namespace LibSerial ;
typedef union
{
  float number;
  uint8_t bytes[4];
} FLOATUNION_t;

class SerialInterface {
 private:
  
  //SerialStream serial_port_ ;
  HardwareSerial* serial_port_ = NULL;
  int c,d;
  int size_;
  float** signal_;
  float** slot_;
  void FloatToByteArray(uint8_t *array, float value, uint16_t *index);
  void CharToByteArray(uint8_t *array, const uint8_t value, uint16_t *index);
  void UIntToByteArray(uint8_t *array, const uint16_t value, uint16_t *index);
  uint8_t CalculateChecksum(uint8_t *buf, int length);
  uint8_t CalculateChecksum(char *buf, int length);
  float ByteArrayToFloat(char *buf,uint16_t *index);
  uint8_t ByteArrayToChar(char *buf,uint16_t *index);
  uint16_t ByteArrayToUInt(char *buf,uint16_t *index);
  uint8_t buffer[100];
  char buf[100];
 public:
 uint16_t cw;
 uint8_t length;
 uint8_t calculated_checksum_;
  uint8_t received_checksum_;
  SerialInterface(HardwareSerial* port, int size);
  ~SerialInterface();
  void setSerialPort(HardwareSerial* port);
  void AddSignal(float* adr);
  void AddSlot(float* adr);
  bool Update();
  
  void Publish(uint16_t msg);
  
};

#endif //SERIAL_INTERFACE__SERIAL_INTERFACE_H_
