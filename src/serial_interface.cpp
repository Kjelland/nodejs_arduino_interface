//
// Created by magnus on 02.11.2021.
//

#include "serial_interface.h"

SerialInterface::SerialInterface(HardwareSerial* port, int size) {
  
  serial_port_ = port;
  this->size_ = size;
  signal_ = new float*[size];
  slot_ = new float*[size];

  c = 0;
  d = 0;
}

SerialInterface::~SerialInterface() {
  if(signal_ != NULL)
    delete[] signal_;
  signal_ = NULL;
  if(slot_ != NULL)
    delete[] slot_;
  slot_ = NULL;
}

void SerialInterface::Publish(uint16_t control_word) {

  uint8_t transmitt_buffer[6 + size_ * 4];
  for (int i = 0; i < sizeof (transmitt_buffer); ++i) {
    transmitt_buffer[i] = 0;
  }
  uint16_t c = 0;
  CharToByteArray(transmitt_buffer,'A',&c);
  CharToByteArray(transmitt_buffer,'B',&c);
  UIntToByteArray(transmitt_buffer,control_word,&c);
  CharToByteArray(transmitt_buffer, size_, &c);
  for (int i = 0; i < size_; ++i) {
    FloatToByteArray(transmitt_buffer, *signal_[i], &c);
  }
  CharToByteArray(transmitt_buffer,CalculateChecksum(transmitt_buffer,sizeof(transmitt_buffer)),&c);
//    CharToByteArray(transmitt_buffer, '/r', &c);
//    CharToByteArray(transmitt_buffer, '/n', &c);
  serial_port_->write(transmitt_buffer,sizeof(transmitt_buffer));
//    for(int i = 0; i < sizeof(transmitt_buffer); i++)
//    {
//        Serial.print(transmitt_buffer[i]);
//    }
  Serial.println();
  // Copy for test
  /*for (int i = 0; i < sizeof (transmitt_buffer); i++) {
    //std::printf("The byte #%d is 0x%02X or %c\n", i, transmitt_buffer[i], transmitt_buffer[i]);
    buffer[i] = transmitt_buffer[i];
  }*/
}

void SerialInterface::UIntToByteArray(uint8_t *array, const uint16_t value, uint16_t *index) {
  array[*index] = (value >> 8) & 0x00FF ;
  array[*index+1] = value & 0x00FF;
  *index += 2;
}
void SerialInterface::CharToByteArray(uint8_t *array, const uint8_t value, uint16_t *index) {
  array[*index] = value;
  *index += 1;
}

uint8_t SerialInterface::ByteArrayToChar(char *buf, uint16_t *index) {
  uint8_t temp = 0;
  temp = buf[*index];
  *index += 1;
  return *((uint8_t *) &temp);
}

float SerialInterface::ByteArrayToFloat(char *buf, uint16_t* index) {
  FLOATUNION_t float_union;
  for (size_t i = 0; i < 4; ++i)
  {
    /* code */
    float_union.bytes[i] = buf[*index + i];
  }
  
  *index += 4;
  return float_union.number;
}

uint16_t SerialInterface::ByteArrayToUInt(char *buf, uint16_t *index) {
  return ByteArrayToChar(buf,index)<<8 | ByteArrayToChar(buf,index);
}

//! Converts a float to 4 bytes and add them to a buffer while increasing the indexing pointer
void SerialInterface::FloatToByteArray(uint8_t *array, float value, uint16_t *index){
  unsigned char const * p = reinterpret_cast<unsigned char const *>(&value);
  FLOATUNION_t myFloat;
  myFloat.number = value;
  for (int i = 0; i != 4; ++i)
  {
    array[i + *index] = myFloat.bytes[i];
  }
  
  
  *index += 4;
}
//
void SerialInterface::AddSignal(float *adr) {
  signal_[c] = adr;
  ++c;
}
void SerialInterface::AddSlot(float *adr) {
  slot_[d] = adr;
  d++;
}
//! Calculate checksum of array based on length
uint8_t SerialInterface::CalculateChecksum(char *buf, int length) {
  uint8_t checksum = 0;
  for (int i = 0; i != length; i++) {
    checksum += buf[i];
  }
  return checksum;
}
//! Calculate checksum of array based on length
uint8_t SerialInterface::CalculateChecksum(uint8_t *buf, int length) {
  uint8_t checksum = 0;
  for (int i = 0; i != length; i++) {
    checksum += buf[i];
  }
  return checksum;
}

bool SerialInterface::Update() {
  uint16_t g = 0;
  
  int n = 0;
  uint32_t timeout = millis() + 5000;
  for (size_t i = 0; i < sizeof buf; i++)
  {
    buf[i] = 0;
  }
  
  while(serial_port_->available() && millis() < timeout){
    buf[0] = serial_port_->read();
    //delay(5);
    if(buf[0] == 'A') {
      while(serial_port_->available()<4){delayMicroseconds(1);}
      buf[1] = serial_port_->read();

      if(buf[1]=='B') {
//          Serial.println("B Received");
        buf[2] = serial_port_->read();  //word upper
//          Serial.print("upper: ");
//          Serial.println(buf[2],DEC);
          buf[3] = serial_port_->read();  //word lower
//          Serial.print("lower:");
//          Serial.println(buf[3],DEC);


          buf[4] = serial_port_->read();  //length
//          Serial.print("len: ");
//          Serial.println(buf[4],DEC);
        while(serial_port_->available()<buf[4]*4 + 1){delayMicroseconds(1);}
        for (n = 0; n < buf[4]*4 + 1; ++n) {
//            Serial.println("try reading float or one crc");

            buf[n+5] = serial_port_->read();
          if(millis() > timeout)
            return false;
        }
      }

    }

  }

  if(ByteArrayToChar(buf,&g)=='A') {
    if (ByteArrayToChar(buf,&g) == 'B') {
      cw = ByteArrayToUInt(buf,&g);
      length = ByteArrayToChar(buf,&g);
      for (int i = 0; i < length; ++i) {
        *slot_[i] = ByteArrayToFloat(buf,&g);
      }
      calculated_checksum_ = CalculateChecksum(buf,g);
      received_checksum_ = ByteArrayToChar(buf,&g);
      if(received_checksum_ == calculated_checksum_ ) {
//          Serial.println("true");
          return true;

      }
      else {
//          Serial.println("false");
          return false;
      }
    }
  } else{
    return false;
  }
}
