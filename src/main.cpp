#include "pins.h"
#include "serial_interface.h"

unsigned long micro_sec_counter;

class TransmittMessage{
public:
    int size = 6;
    uint16_t control_word;
    float x, y, z, roll, pitch, yaw;
};

class ReceiveMessage{
public:
    int size = 6;
    uint16_t status_word;
    float q0,q1,q2,q3,q4,q5;

};

//Receive and parse string var
SerialInterface interface();

void setup()
{

    Serial.begin(115200);
    delay(10);
//    Serial.println("starting");
}

void loop()
{

    SerialInterface interface(&Serial, 6);

    TransmittMessage command;
    ReceiveMessage status;

    interface.AddSlot(&command.x);
    interface.AddSlot(&command.y);
    interface.AddSlot(&command.z);
    interface.AddSlot(&command.roll);
    interface.AddSlot(&command.pitch);
    interface.AddSlot(&command.yaw);

    interface.AddSignal(&status.q0);
    interface.AddSignal(&status.q1);
    interface.AddSignal(&status.q2);
    interface.AddSignal(&status.q3);
    interface.AddSignal(&status.q4);
    interface.AddSignal(&status.q5);

    status.q0 = 1.1;
    status.q1 = 2.2;
    status.q2 = 3.3;
    status.q3 = 4.4;
    status.q4 = 5.5;
    status.q5 = 6.6;

    status.status_word = 1;
    while(status.status_word) {
        if(interface.Update()){
            status.q0 += 1.214;
            status.q1 += 0.41;
            status.q2 += 0.001;
            status.q3 = command.roll;
            status.q4 = command.pitch;
            status.q5 = command.yaw;
            status.status_word +=1;
            interface.Publish(status.status_word);
        }
        delay(100);
    }
}